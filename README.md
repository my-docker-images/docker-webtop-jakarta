
# docker-webtop-jakarta

## Why

This image, based on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop), 
provides an environment for developing [Jakarta EE](https://jakarta.ee) applications using 
[Apache Netbeans](https://netbeans.apache.org/front/main/) and [Eclipse GlassFish](https://glassfish.org).

## Details

- See details on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop).
- See available branches.



